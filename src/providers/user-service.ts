import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UserService {

  constructor(
    private http: HttpClient
  ) {}

  getUsers(latitud, longitud) {
    return this.http.get(' https://api.darksky.net/forecast/ea76e78f539ef7dae1879fd1a45d3628/'+latitud+''+longitud+'');
  }
}

