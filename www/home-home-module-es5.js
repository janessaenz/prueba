(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/@ionic-native/geolocation/index.js":
/*!*********************************************************!*\
  !*** ./node_modules/@ionic-native/geolocation/index.js ***!
  \*********************************************************/
/*! exports provided: Geolocation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Geolocation", function() { return Geolocation; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic-native/core */ "./node_modules/@ionic-native/core/index.js");
/* harmony import */ var rxjs_Observable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Observable */ "./node_modules/rxjs-compat/_esm5/Observable.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * @name Geolocation
 * @description
 * This plugin provides information about the device's location, such as latitude and longitude. Common sources of location information include Global Positioning System (GPS) and location inferred from network signals such as IP address, RFID, WiFi and Bluetooth MAC addresses, and GSM/CDMA cell IDs.
 *
 *  This API is based on the W3C Geolocation API Specification, and only executes on devices that don't already provide an implementation.
 *
 * For iOS you have to add this configuration to your configuration.xml file
 * ```xml
 * <edit-config file="*-Info.plist" mode="merge" target="NSLocationWhenInUseUsageDescription">
 *    <string>We use your location for full functionality of certain app features.</string>
 * </edit-config>
 * ```
 *
 *
 * @usage
 *
 * ```typescript
 * import { Geolocation } from '@ionic-native/geolocation';
 *
 * ...
 *
 * constructor(private geolocation: Geolocation) {}
 *
 * ...
 *
 * this.geolocation.getCurrentPosition().then((resp) => {
 *  // resp.coords.latitude
 *  // resp.coords.longitude
 * }).catch((error) => {
 *   console.log('Error getting location', error);
 * });
 *
 * let watch = this.geolocation.watchPosition();
 * watch.subscribe((data) => {
 *  // data can be a set of coordinates, or an error (if an error occurred).
 *  // data.coords.latitude
 *  // data.coords.longitude
 * });
 * ```
 * @interfaces
 * Coordinates
 * Geoposition
 * PositionError
 * GeolocationOptions
 */
var Geolocation = (function (_super) {
    __extends(Geolocation, _super);
    function Geolocation() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Get the device's current position.
     *
     * @param {GeolocationOptions} options  The [geolocation options](https://developer.mozilla.org/en-US/docs/Web/API/PositionOptions).
     * @returns {Promise<Geoposition>} Returns a Promise that resolves with the [position](https://developer.mozilla.org/en-US/docs/Web/API/Position) of the device, or rejects with an error.
     */
    /**
       * Get the device's current position.
       *
       * @param {GeolocationOptions} options  The [geolocation options](https://developer.mozilla.org/en-US/docs/Web/API/PositionOptions).
       * @returns {Promise<Geoposition>} Returns a Promise that resolves with the [position](https://developer.mozilla.org/en-US/docs/Web/API/Position) of the device, or rejects with an error.
       */
    Geolocation.prototype.getCurrentPosition = /**
       * Get the device's current position.
       *
       * @param {GeolocationOptions} options  The [geolocation options](https://developer.mozilla.org/en-US/docs/Web/API/PositionOptions).
       * @returns {Promise<Geoposition>} Returns a Promise that resolves with the [position](https://developer.mozilla.org/en-US/docs/Web/API/Position) of the device, or rejects with an error.
       */
    function (options) {
        return;
    };
    /**
     * Watch the current device's position.  Clear the watch by unsubscribing from
     * Observable changes.
     *
     * ```typescript
     * const subscription = this.geolocation.watchPosition()
     *                               .filter((p) => p.coords !== undefined) //Filter Out Errors
     *                               .subscribe(position => {
     *   console.log(position.coords.longitude + ' ' + position.coords.latitude);
     * });
     *
     * // To stop notifications
     * subscription.unsubscribe();
     * ```
     *
     * @param {GeolocationOptions} options  The [geolocation options](https://developer.mozilla.org/en-US/docs/Web/API/PositionOptions).
     * @returns {Observable<Geoposition>} Returns an Observable that notifies with the [position](https://developer.mozilla.org/en-US/docs/Web/API/Position) of the device, or errors.
     */
    /**
       * Watch the current device's position.  Clear the watch by unsubscribing from
       * Observable changes.
       *
       * ```typescript
       * const subscription = this.geolocation.watchPosition()
       *                               .filter((p) => p.coords !== undefined) //Filter Out Errors
       *                               .subscribe(position => {
       *   console.log(position.coords.longitude + ' ' + position.coords.latitude);
       * });
       *
       * // To stop notifications
       * subscription.unsubscribe();
       * ```
       *
       * @param {GeolocationOptions} options  The [geolocation options](https://developer.mozilla.org/en-US/docs/Web/API/PositionOptions).
       * @returns {Observable<Geoposition>} Returns an Observable that notifies with the [position](https://developer.mozilla.org/en-US/docs/Web/API/Position) of the device, or errors.
       */
    Geolocation.prototype.watchPosition = /**
       * Watch the current device's position.  Clear the watch by unsubscribing from
       * Observable changes.
       *
       * ```typescript
       * const subscription = this.geolocation.watchPosition()
       *                               .filter((p) => p.coords !== undefined) //Filter Out Errors
       *                               .subscribe(position => {
       *   console.log(position.coords.longitude + ' ' + position.coords.latitude);
       * });
       *
       * // To stop notifications
       * subscription.unsubscribe();
       * ```
       *
       * @param {GeolocationOptions} options  The [geolocation options](https://developer.mozilla.org/en-US/docs/Web/API/PositionOptions).
       * @returns {Observable<Geoposition>} Returns an Observable that notifies with the [position](https://developer.mozilla.org/en-US/docs/Web/API/Position) of the device, or errors.
       */
    function (options) {
        return new rxjs_Observable__WEBPACK_IMPORTED_MODULE_2__["Observable"](function (observer) {
            var watchId = navigator.geolocation.watchPosition(observer.next.bind(observer), observer.next.bind(observer), options);
            return function () { return navigator.geolocation.clearWatch(watchId); };
        });
    };
    Geolocation.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"] },
    ];
    __decorate([
        Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["Cordova"])({
            callbackOrder: 'reverse'
        }),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", Promise)
    ], Geolocation.prototype, "getCurrentPosition", null);
    /**
     * @name Geolocation
     * @description
     * This plugin provides information about the device's location, such as latitude and longitude. Common sources of location information include Global Positioning System (GPS) and location inferred from network signals such as IP address, RFID, WiFi and Bluetooth MAC addresses, and GSM/CDMA cell IDs.
     *
     *  This API is based on the W3C Geolocation API Specification, and only executes on devices that don't already provide an implementation.
     *
     * For iOS you have to add this configuration to your configuration.xml file
     * ```xml
     * <edit-config file="*-Info.plist" mode="merge" target="NSLocationWhenInUseUsageDescription">
     *    <string>We use your location for full functionality of certain app features.</string>
     * </edit-config>
     * ```
     *
     *
     * @usage
     *
     * ```typescript
     * import { Geolocation } from '@ionic-native/geolocation';
     *
     * ...
     *
     * constructor(private geolocation: Geolocation) {}
     *
     * ...
     *
     * this.geolocation.getCurrentPosition().then((resp) => {
     *  // resp.coords.latitude
     *  // resp.coords.longitude
     * }).catch((error) => {
     *   console.log('Error getting location', error);
     * });
     *
     * let watch = this.geolocation.watchPosition();
     * watch.subscribe((data) => {
     *  // data can be a set of coordinates, or an error (if an error occurred).
     *  // data.coords.latitude
     *  // data.coords.longitude
     * });
     * ```
     * @interfaces
     * Coordinates
     * Geoposition
     * PositionError
     * GeolocationOptions
     */
    Geolocation = __decorate([
        Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["Plugin"])({
            pluginName: 'Geolocation',
            plugin: 'cordova-plugin-geolocation',
            pluginRef: 'navigator.geolocation',
            repo: 'https://github.com/apache/cordova-plugin-geolocation',
            install: 'ionic cordova plugin add cordova-plugin-geolocation --variable GEOLOCATION_USAGE_DESCRIPTION="To locate you"',
            installVariables: ['GEOLOCATION_USAGE_DESCRIPTION'],
            platforms: ['Amazon Fire OS', 'Android', 'Browser', 'iOS', 'Windows']
        })
    ], Geolocation);
    return Geolocation;
}(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["IonicNativePlugin"]));

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/home/home.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/home.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n  <div class=\"ion-padding\">\n    <div class=\"container\">\n      <Button class=\"tab-button\"><img src=\"assets/resfr.png\" alt=\"\"></Button>\n      <p class=\"play-frest\"><img src=\"assets/play.png\" alt=\"\">hola mundo</p>\n      <p id=\"geolocation\">Finding geolocation...</p>\n      <h1 class=\"puntaje\">90ª</h1>\n      <div class=\"row\">\n          <div class=\"col-xs-6\">\n              <h1 id=\"nombre\">hola</h1>\n              <p id=\"port\">{{lat}}</p>\n          </div>\n          <div class=\"col-xs-6\">\n              <h1 id=\"nombre1\">ok</h1>\n              <p id=\"port1\">{{lon}}</p>\n          </div>\n      </div>\n      <h1 class=\"final\" id=\"suma\">Summary</h1>\n              <img class=\"footerdos\" src=\"assets/footer.png\" alt=\"\">\n              <p class=\"footerdos\">Copy Janes saenz puerta</p>\n  </div>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/Observable.js":
/*!******************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/Observable.js ***!
  \******************************************************/
/*! exports provided: Observable */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Observable", function() { return rxjs__WEBPACK_IMPORTED_MODULE_0__["Observable"]; });


//# sourceMappingURL=Observable.js.map

/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");







var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
                    }
                ])
            ],
            declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "utton.tab-button.ui-btn.ui-shadow.ui-corner-all img {\n  width: 39px;\n  border-radius: 50%;\n  background: transparent;\n}\n\nbutton.tab-button {\n  width: 100%;\n  background: transparent;\n}\n\nbutton.tab-button img {\n  width: 50px;\n}\n\n.ion-padding, [padding] {\n  background-color: #03c8f5;\n}\n\n.ion-page {\n  background-color: #03c8f5;\n}\n\n.footer {\n  background-color: #03c8f5;\n}\n\np.play-frest {\n  text-align: center;\n  margin-top: 20px;\n  margin-bottom: 20px;\n}\n\n.col-xs-6 {\n  text-align: center;\n  margin-top: 10px;\n  margin-bottom: 10px;\n  width: 50%;\n}\n\np.footerdos {\n  float: right;\n  margin-top: 48px;\n}\n\nimg.footerdos {\n  width: 50px;\n  float: right;\n  margin-top: 30px;\n}\n\nh1#suma {\n  margin-top: 10px;\n  margin-bottom: 10px;\n}\n\n.row {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n}\n\nh1.puntaje {\n  font-size: 100pt;\n  margin-bottom: 30px;\n  margin-top: 30px;\n}\n\np.play-frest img {\n  width: 31px;\n  margin-right: 10px;\n}\n\nbutton.tab-button.ui-btn.ui-shadow.ui-corner-all {\n  background: transparent;\n  border: 0px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy91c3VhcmlvL0Rlc2t0b3AvR2VvY2xpbWEvc3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIiwic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsdUJBQUE7QUNDSjs7QURDQTtFQUNJLFdBQUE7RUFDQSx1QkFBQTtBQ0VKOztBRENBO0VBQ0ksV0FBQTtBQ0VKOztBREFBO0VBQ0kseUJBQUE7QUNHSjs7QUREQTtFQUNJLHlCQUFBO0FDSUo7O0FERkE7RUFDSSx5QkFBQTtBQ0tKOztBREhBO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FDTUo7O0FESkE7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxVQUFBO0FDT0o7O0FETEE7RUFDSSxZQUFBO0VBQ0EsZ0JBQUE7QUNRSjs7QUROQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUNTSjs7QURQQTtFQUNJLGdCQUFBO0VBQ0EsbUJBQUE7QUNVSjs7QURSQTtFQUNJLG9CQUFBO0VBQUEsYUFBQTtFQUNBLHdCQUFBO1VBQUEsdUJBQUE7RUFDQSx5QkFBQTtVQUFBLG1CQUFBO0FDV0o7O0FEVEE7RUFDSSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUNZSjs7QURWQTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtBQ2FKOztBRFhBO0VBQ0ksdUJBQUE7RUFDQSxXQUFBO0FDY0oiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsidXR0b24udGFiLWJ1dHRvbi51aS1idG4udWktc2hhZG93LnVpLWNvcm5lci1hbGwgaW1nIHtcbiAgICB3aWR0aDogMzlweDtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG59XG5idXR0b24udGFiLWJ1dHRvbiB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG59XG5cbmJ1dHRvbi50YWItYnV0dG9uIGltZyB7XG4gICAgd2lkdGg6IDUwcHg7XG59XG4uaW9uLXBhZGRpbmcsIFtwYWRkaW5nXSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzAzYzhmNTtcbn1cbi5pb24tcGFnZSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzAzYzhmNTtcbn1cbi5mb290ZXIge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwM2M4ZjU7XG59XG5wLnBsYXktZnJlc3Qge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG4uY29sLXhzLTYge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgd2lkdGg6IDUwJTtcbn1cbnAuZm9vdGVyZG9zICB7XG4gICAgZmxvYXQ6IHJpZ2h0O1xuICAgIG1hcmdpbi10b3A6IDQ4cHg7XG59XG5pbWcuZm9vdGVyZG9zICB7XG4gICAgd2lkdGg6IDUwcHg7XG4gICAgZmxvYXQ6IHJpZ2h0O1xuICAgIG1hcmdpbi10b3A6IDMwcHg7XG59XG5oMSNzdW1hIHtcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG4ucm93IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG5oMS5wdW50YWplIHtcbiAgICBmb250LXNpemU6IDEwMHB0O1xuICAgIG1hcmdpbi1ib3R0b206IDMwcHg7XG4gICAgbWFyZ2luLXRvcDogMzBweDtcbn1cbnAucGxheS1mcmVzdCBpbWcge1xuICAgIHdpZHRoOiAzMXB4O1xuICAgIG1hcmdpbi1yaWdodDogMTBweDtcbn1cbmJ1dHRvbi50YWItYnV0dG9uLnVpLWJ0bi51aS1zaGFkb3cudWktY29ybmVyLWFsbCB7XG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgYm9yZGVyOiAwcHg7XG59XG4iLCJ1dHRvbi50YWItYnV0dG9uLnVpLWJ0bi51aS1zaGFkb3cudWktY29ybmVyLWFsbCBpbWcge1xuICB3aWR0aDogMzlweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbn1cblxuYnV0dG9uLnRhYi1idXR0b24ge1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG59XG5cbmJ1dHRvbi50YWItYnV0dG9uIGltZyB7XG4gIHdpZHRoOiA1MHB4O1xufVxuXG4uaW9uLXBhZGRpbmcsIFtwYWRkaW5nXSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwM2M4ZjU7XG59XG5cbi5pb24tcGFnZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwM2M4ZjU7XG59XG5cbi5mb290ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDNjOGY1O1xufVxuXG5wLnBsYXktZnJlc3Qge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG5cbi5jb2wteHMtNiB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgd2lkdGg6IDUwJTtcbn1cblxucC5mb290ZXJkb3Mge1xuICBmbG9hdDogcmlnaHQ7XG4gIG1hcmdpbi10b3A6IDQ4cHg7XG59XG5cbmltZy5mb290ZXJkb3Mge1xuICB3aWR0aDogNTBweDtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBtYXJnaW4tdG9wOiAzMHB4O1xufVxuXG5oMSNzdW1hIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cblxuLnJvdyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG5oMS5wdW50YWplIHtcbiAgZm9udC1zaXplOiAxMDBwdDtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbiAgbWFyZ2luLXRvcDogMzBweDtcbn1cblxucC5wbGF5LWZyZXN0IGltZyB7XG4gIHdpZHRoOiAzMXB4O1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbmJ1dHRvbi50YWItYnV0dG9uLnVpLWJ0bi51aS1zaGFkb3cudWktY29ybmVyLWFsbCB7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBib3JkZXI6IDBweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_geolocation__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/geolocation */ "./node_modules/@ionic-native/geolocation/index.js");




var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, geolocation) {
        this.navCtrl = navCtrl;
        this.geolocation = geolocation;
        this.getGeolocation();
    }
    HomePage.prototype.getGeolocation = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (geoposition) {
            _this.lat = geoposition.coords.latitude;
            _this.lon = geoposition.coords.longitude;
        });
    };
    HomePage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicModule"] },
        { type: _ionic_native_geolocation__WEBPACK_IMPORTED_MODULE_3__["Geolocation"] }
    ]; };
    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/index.js!./src/app/home/home.page.html"),
            styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicModule"], _ionic_native_geolocation__WEBPACK_IMPORTED_MODULE_3__["Geolocation"]])
    ], HomePage);
    return HomePage;
}());



/***/ })

}]);
//# sourceMappingURL=home-home-module-es5.js.map